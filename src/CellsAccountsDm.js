import { LitElement, html, } from 'lit-element';
import { BGADPAccountsGetV0 } from '@cells-components/bgadp-accounts-v0'

/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-accounts-dm></cells-accounts-dm>
```

##styling-doc

@customElement cells-accounts-dm
*/
export class CellsAccountsDm extends LitElement {
  static get is() {
    return 'cells-accounts-dm';
  }

 // Declare properties
 static get properties() {
  return {
    host: { type: String },
    version: { type: String },

  };
}

  // Initialize properties
  constructor() {
    super();
    this.host = 'https://cal-glomo.bbva.pe/SRVS_A02';
    this.version = '0';
  }

  generateRequest() {    
    let dp = new BGADPAccountsGetV0({
      host: this.host,
      version: this.version
    });
 
    dp.generateRequest()
      .then(success => {
        console.log('success',success);
        this._onRequestSuccess(success);
      })
      .catch((error) => {
        console.log('error',error);
        this._onRequestError(error);
     });
  }

  _onRequestSuccess({response}) {
    this._dispatchEvent("request-success", response);
  }
 
  _onRequestError({response}) {
    this._dispatchEvent("request-error", response);
  }

  _dispatchEvent(event, obj) {
    this.dispatchEvent(new CustomEvent(event, {
      bubbles: true,
      composed: true,
      detail: obj,
    }));
  }
}
